export class EventsToolkit {

  static preventAll (e: Event) {
    e.stopPropagation()
    e.preventDefault()
    e.cancelBubble = true
    e.returnValue = false
  }
}

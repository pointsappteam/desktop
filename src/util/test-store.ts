import Vue from 'vue'
import { State as AddFormState } from '../store/modules/todo-add-form/state'
import { todoAddForm } from '../store/modules/todo-add-form'
import { makeTodoList } from '../store/modules/todo-list'
import Vuex from 'vuex'
import { mockApi } from '../model/api'

export const createStore = () => {

  Vue.use(Vuex)

  todoAddForm.state = new AddFormState()

  return new Vuex.Store({
    modules: {
      todoAddForm,
      todoList: makeTodoList(mockApi)
    },
    strict: false
  })

}

import { State } from './state'
import { getters } from './getters'
import { mutations } from './mutations'
import { actions } from './actions'

export const todoAddForm = {
  state: new State(),
  getters: getters,
  mutations: mutations,
  actions: actions
}

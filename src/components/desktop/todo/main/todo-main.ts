import { Component, Vue } from 'vue-property-decorator'
import { TodoEditorComponent } from '../editor'
import { TodoListComponent } from '../list'
import { TodoAddFormComponent } from '../add-from'
import { Logger } from '../../../../util/log'

import './todo-main.scss'

@Component({
  template: require('./todo-main.html'),
  components: {
    'editor'  : TodoEditorComponent,
    'list'    : TodoListComponent,
    'add-form': TodoAddFormComponent
  }
})

export class TodoMainComponent extends Vue {

  protected logger: Logger

  mounted () {
    if (!this.logger) this.logger = new Logger()
    this.$nextTick(() => this.logger.info('todo main is loaded'))
  }
}

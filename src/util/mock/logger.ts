import {ILogger} from "../log";
import {SinonSpy} from "sinon";

export class MockLogger implements ILogger {

  constructor (private loggerSpy: SinonSpy) {
  }

  info (msg: any) {
    this.loggerSpy(msg)
  }

  warn (msg: any) {
    this.loggerSpy(msg)
  }

  error (msg: any) {
    this.loggerSpy(msg)
  }
}

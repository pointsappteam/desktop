import { spy, assert } from 'sinon'
import { expect } from 'chai'
import Component from 'vue-class-component'
import { ComponentTest } from '../../../util/component-test'
import { MockLogger } from "../../../util/mock";
import {LeftmenuComponent} from "./leftmenu";

let loggerSpy = spy()

@Component({
  template: require('./leftmenu.html')
})
class MockLeftmenuComponent extends LeftmenuComponent {
  constructor () {
    super()
    this.logger = new MockLogger(loggerSpy)
  }
}

describe('Left menu component', () => {
  let directiveTest: ComponentTest

  beforeEach(() => {
    directiveTest = new ComponentTest('<div><testcomponent></testcomponent></div>', { 'testcomponent': MockLeftmenuComponent })
  })

  it('should render correct contents', async () => {
    debugger
    directiveTest.createComponent()

    await directiveTest.execute((vm) => {
      assert.calledWith(loggerSpy, 'leftmenu is loaded')
    })
  })
})

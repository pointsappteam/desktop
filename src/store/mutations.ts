export const TodoAddFormMutations = {
  SHOW: 'todoAddFormShow',
  HIDE: 'todoAddFormHide',
  UPDATE_MESSAGE: 'todoAddFormUpdateMessage'
}

export const TodoListMutations = {
  ADD: 'todoListAddItem',
  CLEAR: 'todoListClear',
  ACTIVATE: 'todoListActivateItem',
  DEACTIVATE: 'todoListDeactivateItem',
  DEACTIVATE_ALL: 'todoListDeactivateAll',
  TOGGLE: 'todoListToggleItem',
  TOGGLE_GROUP: 'todoListToggleItemGroup',
  SWAP: 'todoListSwapItems',
  DRAG_START: 'todoListDragStart',
  DRAG_FINISH: 'todoListDragFinish'
}

export const AuthMutations = {
  UPDATE_TOKEN: 'authSetToken'
}

export const AuthSignupFormMutations = {
  UPDATE_EMAIL: 'authSignupUpdateEmail',
  UPDATE_PASSWORD: 'authSignupUpdatePassword'
}

import { ListItem } from '../../../model/todo/list-item'
import { ApiInterface } from '../../../model/api/api-interface'

export class TodoListState {
  items: Array<ListItem> = []
}

export class State {
  todoList = {
    items: [
      // { id:  1, title: ' 1 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id:  2, title: ' 2 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id:  3, title: ' 3 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id:  4, title: ' 4 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id:  5, title: ' 5 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id:  6, title: ' 6 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id:  7, title: ' 7 points - сделать первую юзер-историю points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id:  8, title: ' 8 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id:  9, title: ' 9 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id: 10, title: '10 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id: 11, title: '11 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id: 12, title: '12 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id: 13, title: '13 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id: 14, title: '14 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id: 15, title: '15 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id: 16, title: '16 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id: 17, title: '17 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id: 18, title: '18 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id: 19, title: '19 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id: 20, title: '20 points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id: 21, title: 'points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id: 22, title: 'points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id: 23, title: 'points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id: 24, title: 'points - сделать первую юзер-историю', isActive: false, isDragged: false },
      // { id: 25, title: 'points - сделать первую юзер-историю', isActive: false, isDragged: false }
    ]
  } as TodoListState
  api: ApiInterface

  constructor (api: ApiInterface) {
    this.api = api
  }

  findIndexById (id: number): number {
    let activatedItem = this.todoList.items.find((item: ListItem) => { return item.id === id })
    return this.todoList.items.indexOf(activatedItem)
  }

  toggleById (id: number) {
    let index = this.findIndexById(id)
    this.todoList.items[index].isActive = !this.todoList.items[index].isActive
  }

  toggleExclusiveById (id: number) {
    let index = this.findIndexById(id)

    this.deactivateAll()

    this.todoList.items[index].isActive = !this.todoList.items[index].isActive
  }

  setActiveById (id: number, active: boolean) {
    this.todoList.items[this.findIndexById(id)].isActive = active
  }

  deactivateAll () {
    for (let i in this.todoList.items) {
      this.todoList.items[i].isActive = false
    }
  }

  swap (xId: number, yId: number) {
    let xIndex = this.findIndexById(xId)
    let yIndex = this.findIndexById(yId)

    if (xIndex === yIndex) {
      return
    }

    if (yIndex > xIndex) {
      for (let i = yIndex; i > xIndex; i--) {
        this.todoList.items[i - 1] = this.todoList.items.splice(i, 1, this.todoList.items[i - 1])[0]
      }

      return
    }

    for (let i = yIndex; i < xIndex; i++) {
      this.todoList.items[i] = this.todoList.items.splice(i + 1, 1, this.todoList.items[i])[0]
    }
  }

  toggleDrag (id: number, isDragged: boolean) {
    this.todoList.items[this.findIndexById(id)].isDragged = isDragged
  }
}

import { TokenStorageInterface } from './token-storage-interface'

export class TokenLocalStorage implements TokenStorageInterface {

  get (): string {
    return localStorage.getItem('authToken') || ''
  }

  store (token: string) {
    localStorage.setItem('authToken', token)
  }
}

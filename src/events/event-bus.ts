import Vue from 'vue'

export const EventBus = new Vue()

export const TodoAddFormEvents = {
  OPEN: 'todoAddFormOpen'
}

export const TodoListEvents = {
  ADDED: 'todoListItemAdded'
}

export const AuthEvents = {
  AUTHORISED: 'authorised',
  NOT_AUTHORISED: 'notAuthorised'
}

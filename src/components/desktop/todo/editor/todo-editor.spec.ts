import { spy, assert } from 'sinon'
import { expect } from 'chai'
import Component from 'vue-class-component'
import { ComponentTest } from '../../../../util/component-test'
import { MockLogger } from "../../../../util/mock"
import {TodoEditorComponent} from "./todo-editor"

let loggerSpy = spy()

@Component({
  template: require('./todo-editor.html')
})
class MockTodoEditorComponent extends TodoEditorComponent {
  constructor () {
    super()
    this.logger = new MockLogger(loggerSpy)
  }
}

describe('Todo editor component', () => {
  let directiveTest: ComponentTest

  beforeEach(() => {
    directiveTest = new ComponentTest('<div><todo-editor></todo-editor></div>', { 'todo-editor': MockTodoEditorComponent })
  })

  it('should render correct contents', async () => {
    debugger
    directiveTest.createComponent()

    await directiveTest.execute((vm) => {
      assert.calledWith(loggerSpy, 'todo editor is loaded')
    })
  })
})

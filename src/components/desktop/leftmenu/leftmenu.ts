import { Component, Vue } from 'vue-property-decorator'
import { Logger } from '../../../util/log'

import './leftmenu.scss'

@Component({
  template: require('./leftmenu.html'),
  components: {}
})

export class LeftmenuComponent extends Vue {

  protected logger: Logger

  mounted () {
    if (!this.logger) this.logger = new Logger()
    this.$nextTick(() => this.logger.info('leftmenu is loaded'))
  }

}

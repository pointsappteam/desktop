export class IdentityGenerator {

  private ids: Array<number> = []

  // constructor () {
  //   this.ids['todo'] = 25
  // }

  generate (name: string): number {
    if (this.ids[name] === undefined) {
      this.ids[name] = 0
    }

    this.ids[name]++

    return this.ids[name]
  }

  reset (name: string) {
    this.ids[name] = 0
  }
}

export const idGenerator = new IdentityGenerator()

## Веб-версия для [десктопа](./requirements/desktop)

Требование | Текст | Дизайн | Код | Релиз  
--- | --- | --- | --- | ---
[Регистрация](./requirements/desktop/signup.md) |+|+|-|-
[Цвета и формы поинта](./requirements/desktop/shapes-and-colors.md) | + | + | - | -
[Перетаскивание поинта в списке](./requirements/desktop/drag.md) | + | + | +\\- | - 
[Список поинтов](./requirements/desktop/list.md) | + | + | + | +
[Форма создания поинта](./requirements/desktop/Forms/Add%20point.md) | + | + | + | +
[Главный экран](./requirements/desktop/Layout.md) |+|+|+|+
[HTML](./requirements/desktop/HTML.md) |+|+|+|+

### Полезное

[Клавиатурные сочетания](./requirements/desktop/Global%20shortcuts.md)

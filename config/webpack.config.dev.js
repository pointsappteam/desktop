const DefinePlugin = require('webpack/lib/DefinePlugin')
const webpackConfig = require('./webpack.config.dev.base')
const env = require('../environment/dev.env')

webpackConfig.plugins = [...webpackConfig.plugins,
    new DefinePlugin({
        'process.env': env
    })
]

module.exports = webpackConfig

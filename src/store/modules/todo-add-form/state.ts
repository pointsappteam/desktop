export class AddFormState {
  active: boolean
  title: string
}

export class State {
  todoAddForm = {
    active: false,
    title: ''
  } as AddFormState
}

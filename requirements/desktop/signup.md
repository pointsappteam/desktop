#Регистрация пользователя

Чтобы попасть на страницу регистрации, надо нажать на кнопку Sign Up на странице лендинга. Регистрация бесплатная.

Есть два варианта регистрации:

1. Через почту.
2. Через аккаунт в Фейсбуке.

##Регистрация через почту
Чтобы зарегистрироваться через почту, надо заполнить два поля:

1. Email.
2. Password.

Если введен валидный адрес эл. почты, то емоджи :thumbsup: становится цветным. 
Минимальная длина пароля — 8 любых символов. Емоджи :muscle: становится цветным, как только это условие выполнятеся.

В поле ввода пароля есть счетчик введенных символов. Можно набрать и больше восьми символов. 

Над полем ввода пароля есть кнопка Show / Hide, чтобы посмотреть, какие символы уже введены. 

Как только все условия выполнились, кнопка Get Started! раздизабливается и на нее можно нажать и зарегистрироваться.

Когда нажимаем на Get Started! на кнопке появляется спиннер. Он просто крутится. 

Пользователя перенаправляем в его аакаунт = первая страница без поинтов, как только смогли его зарегистрировать.

###Подтверждение почты
После того, как пользователь нажал на Get Started! на указанную им почту приходит письмо:
Subject: Verify your Points account

Welcome to Points.
Please verify your email address by clicking on this link:  
<link>

Cheers,  
The Points Team

После нажатия на ссылку пользователь попадает в свой аккаунт.

###Возможные ошибки
При регистрации могут возникнуть ошибки:

1. Такой пользователь уже зарегистрирован — показываем красный текст под полем и красим красным рамку поля. 
2. Пользователь в оффлайне — показываем желтую плашку сверху по ширине всего экрана. Кнопка Retry несет ту же функцию, что и кнопка Get Started!
3. У нас что-то сломалось, но пользователь в онлайне — показываем желтую плашку на всю ширину экрана с общим текстом и кнопкой для перезагрузки страницы. 

##Регистрация через Фейсбук
Чтобы зарегистрироваться через Фейсбук, надо нажать на кнопку Sign Up with Facebook.

Чтобы внести логин и пароль и подтвердить разрешение на доступ к данным открываем отдельные небольшие окна поверх окна сайнапа. Не редиректим.  

Когда нажимаем на Sign Up with Facebook на кнопке появляется спиннер. Он просто крутится.

###Возможные ошибки
При регистрации через Фейсбук могут возникнуть ошибки:

1. Такой пользователь уже зарегистрирован — показываем красный текст под кнопкой и красим красным рамку кнопки. 
2. Зарегистрироваться через Фейсбук не удалось по какой-либо технической причине — показываем красный текст под кнопкой и красим красным рамку кнопки.
3. Пользователь в оффлайне — показываем желтую плашку сверху по ширине всего экрана. Кнопка Retry несет ту же функцию, что и кнопка Sign Up with Facebook.
4. У нас что-то сломалось, но пользователь в онлайне — показываем желтую плашку на всю ширину экрана с общим текстом и кнопкой для перезагрузки страницы.
import { Component, Vue } from 'vue-property-decorator'
import { AuthSignupFormMutations } from '../../../../store/mutations'
import { EventBus, AuthEvents } from '../../../../events/event-bus'
import { mapActions, mapMutations } from 'vuex'
import { Getter } from 'vuex-class'

import './signup.scss'

@Component({
  template: require('./signup.html'),
  components: {},
  methods: {
    ...mapActions(['send'])
  },
  directives: {
  }
})

export class AuthSignupComponent extends Vue {

  @Getter signupEmail
  @Getter signupPassword
  @Getter signupFormValid

  updateEmail (e) {
    let payload = {
      value: e.target.value,
      validity: e.target.checkValidity()
    }
    this.$store.commit(AuthSignupFormMutations.UPDATE_EMAIL, payload)
  }

  updatePassword (e) {
    this.$store.commit(AuthSignupFormMutations.UPDATE_PASSWORD, e.target.value)
  }

}

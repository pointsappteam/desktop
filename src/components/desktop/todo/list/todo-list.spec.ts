import { spy, assert } from 'sinon'
import { expect } from 'chai'
import Component from 'vue-class-component'
import { ComponentTest, IComponents } from '../../../../util/component-test'
import { TodoListComponent } from './todo-list'
import { idGenerator } from '../../../../util/id-generator'
import { TodoListMutations } from '../../../../store/mutations'
import { ListItem } from '../../../../model/todo/list-item'

@Component({
  template: require('./todo-list.html')
})
class MockTodoListComponent extends TodoListComponent {
  constructor () {
    super()
  }
}

describe('Todo list component', () => {
  let directiveTest: ComponentTest

  beforeEach(async () => {
    directiveTest = new ComponentTest(
      '<div><testcomponent></testcomponent></div>',
      { 'testcomponent': MockTodoListComponent } as IComponents
    )

    directiveTest.createComponent()

    await directiveTest.execute((vm) => {
      vm.$el.style.height = '400px'
      let container = document.createElement('div') as HTMLDivElement
      container.style.height = '400px'
      container.appendChild(vm.$el)
      document.body.appendChild(container)
    })
  })

  afterEach(() => {
    idGenerator.reset('todo')
    directiveTest.vm.$store.commit(TodoListMutations.CLEAR)
    directiveTest.destroy();
  })

  describe('List rendered correctly', () => {

    it('placeholder if list empty', async () => {

      await directiveTest.execute((vm) => {
        let placeholder = vm.$el.querySelector('.todo-list-empty-placeholder')
        expect(placeholder).to.exist
        let anyItem = vm.$el.querySelector('.todo-list-item')
        expect(anyItem).to.not.exist
      })
    })

    it('no placeholder if list not empty', async () => {

      directiveTest.vm.$store.commit(
        TodoListMutations.ADD,
        <ListItem>{id: idGenerator.generate('todo'), isActive: false, title: 'simple test point'}
      )

      await directiveTest.execute((vm) => {
        let placeholder = vm.$el.querySelector('.todo-list-empty-placeholder')
        expect(placeholder).to.not.exist
        let anyItem = vm.$el.querySelector('.todo-list-item')
        expect(anyItem).to.exist
      })
    })
  })

  describe('Activation works correctly', () => {

    beforeEach(async () => {
      directiveTest.vm.$store.commit(
        TodoListMutations.ADD,
        <ListItem>{id: idGenerator.generate('todo'), isActive: false, title: 'simple test point'}
      )
    })

    it( 'click activates point', async () => {
      let items = directiveTest.vm.$el.querySelectorAll('.todo-list-item')

      await directiveTest.execute((vm) => {
        (items.item(0) as HTMLDivElement).click()
      })

      await directiveTest.execute((vm) => {
        expect(items.item(0).classList.contains('active')).to.eql(true)
      })
    })

    it( 'click activates point exclusively', async () => {
      await directiveTest.execute((vm) => {
        directiveTest.vm.$store.commit(
          TodoListMutations.ADD,
          <ListItem>{id: idGenerator.generate('todo'), isActive: false, title: 'simple test point'}
        )
      })

      let items = directiveTest.vm.$el.querySelectorAll('.todo-list-item')

      await directiveTest.execute((vm) => {
          (items.item(0) as HTMLDivElement).click()
      })

      await directiveTest.execute((vm) => {
        (items.item(1) as HTMLDivElement).click()
      })

      await directiveTest.execute((vm) => {
        expect(items.item(0).classList.contains('active')).to.eql(false)
        expect(items.item(1).classList.contains('active')).to.eql(true)
      })
    })

    it( 'ctrl click activates point non-exclusively', async () => {
      await directiveTest.execute((vm) => {
        directiveTest.vm.$store.commit(
          TodoListMutations.ADD,
          <ListItem>{id: idGenerator.generate('todo'), isActive: false, title: 'simple test point'}
        )
      })

      let items = directiveTest.vm.$el.querySelectorAll('.todo-list-item')

      await directiveTest.execute((vm) => {
        (items.item(0) as HTMLDivElement).click()
      })

      //click with ctrl
      await directiveTest.execute((vm) => {
        let item = items.item(1) as HTMLDivElement;
        let event = document.createEvent('MouseEvent') as MouseEvent
        event.initMouseEvent(
          'click',
          false,
          false,
          window,
          0,
          0,
          0,
          item.clientLeft,
          item.clientTop,
          true,
          false,
          false,
          false,
          0,
          null
        )
        item.dispatchEvent(event)
      })

      await directiveTest.execute((vm) => {
        expect(items.item(0).classList.contains('active')).to.eql(true)
        expect(items.item(1).classList.contains('active')).to.eql(true)
      })
    })

    it( 'click outside deactivates point', async () => {
      let items = directiveTest.vm.$el.querySelectorAll('.todo-list-item')

      await directiveTest.execute((vm) => {
        (items.item(0) as HTMLDivElement).click()
      })

      //click outside
      await directiveTest.execute((vm) => {
        let fakeElement = document.createElement('DIV')
        document.body.appendChild(fakeElement)
        fakeElement.click()
      })

      await directiveTest.execute((vm) => {
        expect(items.item(0).classList.contains('active')).to.eql(false)
      })
    })
  })

})

import { State } from './state'
import { getters } from './getters'
import { mutations } from './mutations'
import { ActionContext } from 'vuex'
import axios from 'axios'
import { TodoListMutations } from '../../mutations'
import { ListItem } from '../../../model/todo/list-item'

export function makeTodoList (api) {
  return {
    state: new State(api),
    getters: getters,
    mutations: mutations,
    actions: {
      loadPointsList (store: ActionContext<State, any>) {
        axios.get(store.state.api.points.list).then(
          (response) => {
            let items = response.data.items || []
            items.forEach((item) => {
              const listItem = {
                id: parseInt(item.id, 0),
                title: item.title,
                isActive: false,
                isDragged: false
              } as ListItem

              store.commit(TodoListMutations.ADD, listItem)
            })
          },
          (error) => { console.log(error) }
        )
      }
    }
  }
}

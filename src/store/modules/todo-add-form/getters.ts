import { GetterTree } from 'vuex'
import { State } from './state'

export const getters = {
  addFormActive (state: State): boolean {
    return state.todoAddForm.active
  },
  addFormTitle (state: State): string {
    return state.todoAddForm.title
  }
} as GetterTree<State, any>

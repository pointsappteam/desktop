import { spy, assert } from 'sinon'
import { expect } from 'chai'
import { mutations } from './mutations'
import { State } from './state'
import { TodoListMutations } from '../../mutations'
import { EventBus, TodoListEvents } from '../../../events/event-bus'
import { ListItem } from '../../../model/todo/list-item'
import { mockApi } from '../../../model/api'

const {
  [TodoListMutations.ADD]: add
} = mutations

describe('Todo list Store module', () => {

  describe('Mutations', () => {

    it('add', () => {
      let eventBusSpy = spy(EventBus, '$emit')

      const state = {todoList: {items: []}} as State

      add(state, {id: 1, isActive: false, title: 'new point'} as ListItem)

      expect(state.todoList.items.length).to.equal(1)
      expect(eventBusSpy.calledWith(TodoListEvents.ADDED)).to.equal(true)

      eventBusSpy.restore()
    })
  })

  describe('State', () => {
    it('find', () => {
      const state = new State(mockApi)
      state.todoList.items = [
            {id: 2, isActive: false, title: '2', isDragged: false},
            {id: 3, isActive: false, title: '3', isDragged: false},
      ]

      expect(state.findIndexById(3)).to.equal(1)
    })

    it('toggleOne', () => {
      const state = new State(mockApi)
      state.todoList.items = [
            {id: 2, isActive: false, title: '2', isDragged: false},
            {id: 3, isActive: true, title: '3', isDragged: false},
      ]

      state.toggleById(2)
      expect(state.todoList.items[0].isActive).to.equal(true)

      state.toggleById(3)
      expect(state.todoList.items[1].isActive).to.equal(false)
    })

    it('toggleExclusive', () => {
      const state = new State(mockApi)
      state.todoList.items = [
        {id: 2, isActive: false, title: '2', isDragged: false},
        {id: 3, isActive: true, title: '3', isDragged: false},
      ]

      state.toggleExclusiveById(2)
      expect(state.todoList.items[0].isActive).to.equal(true)
      expect(state.todoList.items[1].isActive).to.equal(false)
    })

    it('activate', () => {
      const state = new State(mockApi)
      state.todoList.items = [
        {id: 2, isActive: false, title: '2', isDragged: false},
        {id: 3, isActive: true, title: '3', isDragged: false},
      ]

      state.setActiveById(2, true)
      expect(state.todoList.items[0].isActive).to.equal(true)
      expect(state.todoList.items[1].isActive).to.equal(true)

      state.setActiveById(3, false)
      expect(state.todoList.items[0].isActive).to.equal(true)
      expect(state.todoList.items[1].isActive).to.equal(false)
    })

    it('deactivate', () => {
      const state = new State(mockApi)
      state.todoList.items = [
        {id: 2, isActive: false, title: '2', isDragged: false},
        {id: 3, isActive: true, title: '3', isDragged: false},
      ]

      state.deactivateAll()
      expect(state.todoList.items[0].isActive).to.equal(false)
      expect(state.todoList.items[1].isActive).to.equal(false)
    })
  })
})

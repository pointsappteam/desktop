import Vue from 'vue'
import VueRouter, { Location, Route, RouteConfig } from 'vue-router'
import { makeHot, reload } from './util/hot-reload'
import { Platform } from './util/platform'

const todoMainComponent = () => import('./components/desktop/todo/main').then(({ TodoMainComponent }) => TodoMainComponent)
const authSignupComponent = () => import('./components/desktop/auth/signup').then(({ AuthSignupComponent }) => AuthSignupComponent)

if (process.env.ENV === 'development' && module.hot) {
  const todoMainModuleId = './components/desktop/todo/main'
  const authSignupModuleId = './components/desktop/auth/signup'

  // first arguments for `module.hot.accept` and `require` methods have to be static strings
  // see https://github.com/webpack/webpack/issues/5668
  makeHot(todoMainModuleId, todoMainComponent,
    module.hot.accept('./components/desktop/todo/main', () => reload(todoMainModuleId, (require('./components/desktop/todo/main') as any).TodoMainComponent)))
  makeHot(authSignupModuleId, authSignupComponent,
    module.hot.accept('./components/desktop/todo/main', () => reload(authSignupModuleId, (require('./components/desktop/auth/signup') as any).AuthSignupComponent)))
}

Vue.use(VueRouter)

export const createRoutesDesktop: () => RouteConfig[] = () => [
  {
    name: 'home',
    path: '/',
    component: todoMainComponent
  },
  {
    name: 'signup',
    path: '/signup/',
    component: authSignupComponent
  }
]

export const createRoutesMobile: () => RouteConfig[] = () => []

export const createRouter = () => new VueRouter({ mode: 'history', routes: Platform.isMobile() ? createRoutesMobile() : createRoutesDesktop() })

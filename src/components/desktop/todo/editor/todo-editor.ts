import { Component, Vue } from 'vue-property-decorator'
import { Logger } from '../../../../util/log'

import './todo-editor.scss'

@Component({
  template: require('./todo-editor.html'),
  components: {}
})

export class TodoEditorComponent extends Vue {

  protected logger: Logger

  mounted () {
    if (!this.logger) this.logger = new Logger()
    this.$nextTick(() => this.logger.info('todo editor is loaded'))
  }

}

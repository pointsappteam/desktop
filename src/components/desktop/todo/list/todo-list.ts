import { Component, Vue } from 'vue-property-decorator'
import { Getter } from 'vuex-class'
import { ListItem } from '../../../../model/todo/list-item'
import { TodoListMutations } from '../../../../store/mutations'
import { EventBus, TodoListEvents } from '../../../../events/event-bus'
import { ClickOutsideDirective } from '../../../../util/directives/click-outside'
import { HtmlId } from '../../../../util/html-id'
import { TodoListDraggedItem } from './dragged-item'

import './todo-list.scss'

@Component({
  template: require('./todo-list.html'),
  components: {},
  directives: {
    'click-outside': ClickOutsideDirective
  }
})

export class TodoListComponent extends Vue {
  @Getter todoListItems: Array<ListItem>

  protected doHasScroll: boolean = false

  private idParser = new HtmlId('m-todo-id-')
  private movedItem: TodoListDraggedItem

  onActivate (e: Event) {
    this.$store.commit(TodoListMutations.TOGGLE, this.idParser.parseFromElement(e.target as HTMLDivElement))
  }

  onActivateCtrl (e: Event) {
    this.$store.commit(TodoListMutations.TOGGLE_GROUP, this.idParser.parseFromElement(e.target as HTMLDivElement))
  }

  onAdded () {
    this.$nextTick(() => {
      this.$el.scrollTop = this.$el.scrollHeight
    })
  }

  onScroll () {
    this.updateHasScroll()
  }

  onDeselectAll () {
    this.$store.commit(TodoListMutations.DEACTIVATE_ALL)
  }

  onMoveStart (e: MouseEvent) {
    this.movedItem.onMoveStart(e)
  }

  get hasScroll (): boolean {
    return this.doHasScroll
  }

  mounted () {
    EventBus.$on(TodoListEvents.ADDED, this.onAdded)

    this.movedItem = new TodoListDraggedItem(this.$store.commit, this.idParser)

    this.$store.dispatch('loadPointsList')

    this.$nextTick(() => {
      this.updateHasScroll()
      window.addEventListener('resize', this.updateHasScroll)
    })
  }

  beforeDestroy () {
    EventBus.$off(TodoListEvents.ADDED)
    window.removeEventListener('resize', this.updateHasScroll)
  }

  private updateHasScroll () {
    this.doHasScroll = this.$el.scrollHeight > this.$el.clientHeight
  }
}

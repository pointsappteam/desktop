import { spy, assert } from 'sinon'
import { expect } from 'chai'
import Component from 'vue-class-component'
import { ComponentTest } from '../../../../util/component-test'
import { MockLogger, MockGenericComponent } from "../../../../util/mock";
import {TodoMainComponent} from "./todo-main";

let loggerSpy = spy()

@Component({
  template: require('./todo-main.html'),
  components: {
    'editor'  : MockGenericComponent,
    'list'    : MockGenericComponent,
    'add-form': MockGenericComponent
  }
})
class MockTodoMainComponent extends TodoMainComponent {
  constructor () {
    super()
    this.logger = new MockLogger(loggerSpy)
  }
}

describe('Todo main component', () => {
  let directiveTest: ComponentTest

  beforeEach(() => {
    directiveTest = new ComponentTest('<div><testcomponent></testcomponent></div>', { 'testcomponent': MockTodoMainComponent })
  })

  it('should render correct contents', async () => {
    debugger
    directiveTest.createComponent()

    await directiveTest.execute((vm) => {
      assert.calledWith(loggerSpy, 'todo main is loaded')
    })
  })
})

import Vue, { Component } from 'vue'
import { SinonSpy } from 'sinon'
import merge from 'lodash.merge'
import { ILogger } from './log'
import { createStore } from './test-store'

export interface IComponents {
  [key: string]: Component
}

export class ComponentTest {

  public vm: Vue

  constructor (private template: string, private components: IComponents) {
  }

  public createComponent (createOptions?: any): void {
    let options = {
      template: this.template,
      components: this.components,
      store: createStore()
    }
    if (createOptions) merge(options, createOptions)
    this.vm = new Vue(options).$mount()
  }

  public async execute (callback: (vm: Vue) => Promise<void> | void): Promise<void> {
    await Vue.nextTick()
    await callback(this.vm)
  }

  public destroy () {
    this.vm.$destroy()
  }

}

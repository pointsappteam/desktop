import { ApiInterface } from '../../../model/api/api-interface'

export class SignupFormState {
  email: string
  password: string
  isEmailValid: boolean
  isPasswordStrong: boolean
  isFormSending: boolean
}

export class State {
  authSignupForm = {
    email: '',
    password: '',
    isEmailValid: false,
    isPasswordStrong: false,
    isFormSending: false
  } as SignupFormState
  api: ApiInterface

  constructor (api: ApiInterface) {
    this.api = api
  }
}

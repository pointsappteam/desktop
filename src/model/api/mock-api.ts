import { ApiInterface, ResourceApiInterface } from './api-interface'

class MockApi implements ApiInterface {
  signup: string = 'https://demo2132250.mockable.io/api/signup'
  points = {
    list: 'http://demo2132250.mockable.io/api/points'
  } as ResourceApiInterface
}

export const mockApi = new MockApi()

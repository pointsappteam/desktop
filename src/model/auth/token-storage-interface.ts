export interface TokenStorageInterface {
  store(token: string)
  get(): string
}

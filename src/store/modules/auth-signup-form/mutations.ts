import { MutationTree } from 'vuex'
import { AuthSignupFormMutations } from '../../mutations'
import { State } from './state'

export const mutations = {
  [AuthSignupFormMutations.UPDATE_EMAIL] (state: State, emailState) {
    state.authSignupForm.email = emailState.value
    state.authSignupForm.isEmailValid = emailState.validity
  },
  [AuthSignupFormMutations.UPDATE_PASSWORD] (state: State, password: string) {
    state.authSignupForm.password = password
    state.authSignupForm.isPasswordStrong = (password.length >= 8)
  }
} as MutationTree<State>

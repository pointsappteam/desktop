import { spy, assert } from 'sinon'
import { expect } from 'chai'
import { mutations } from './mutations'
import { actions } from './actions'
import { State } from './state'
import { TodoAddFormMutations, TodoListMutations } from '../../mutations'
import { EventBus, TodoAddFormEvents } from '../../../events/event-bus'
import { testAction } from '../../../util/store-action-test'
import { idGenerator } from '../../../util/id-generator'

const {
  [TodoAddFormMutations.SHOW]: show,
  [TodoAddFormMutations.HIDE]: hide,
  [TodoAddFormMutations.UPDATE_MESSAGE]: updateMessage
} = mutations

describe('Todo add form Store module', () => {

  describe('Mutations', () => {

    it('show', () => {
      let eventBusSpy = spy(EventBus, '$emit')

      const state = {todoAddForm: {active: false, title: ''}} as State

      show(state, {})

      expect(state.todoAddForm.active).to.equal(true)
      expect(eventBusSpy.calledWith(TodoAddFormEvents.OPEN)).to.equal(true)

      eventBusSpy.restore()
    })

    it('hide', () => {
      const state = {todoAddForm: {active: true, title: ''}} as State

      hide(state, false)

      expect(state.todoAddForm.active).to.equal(false)
    })

    it('hide and clear', () => {
      const state = {todoAddForm: {active: true, title: 'title to clear'}} as State

      hide(state, true)

      expect(state.todoAddForm.active).to.equal(false)
      expect(state.todoAddForm.title).to.equal('')
    })

    it('update message', () => {
      const state = {todoAddForm: {active: true, title: ''}} as State
      const newMessage = 'new message'

      updateMessage(state, newMessage)

      expect(state.todoAddForm.title).to.equal(newMessage)
    })
  })

  describe('Actions', () => {

    it('open', done => {
      testAction(actions.open, null, {}, [
        {type: TodoAddFormMutations.SHOW}
      ], done)
    })

    it('close', done => {
      testAction(actions.close, null, {}, [
        {type: TodoAddFormMutations.HIDE, payload: false}
      ], done)
    })

    it('closeClear', done => {
      testAction(actions.closeClear, null, {}, [
        {type: TodoAddFormMutations.HIDE, payload: true}
      ], done)
    })

    it('save', done => {
      const state = {todoAddForm: {active: true, title: 'new point'}} as State
      const emptyState = {todoAddForm: {active: true, title: ''}} as State

      const item = {id: 1, title: 'new point', isActive: false, isDragged: false}

      idGenerator.reset('todo')
      testAction(actions.save, null, state, [
        {type: TodoListMutations.ADD, payload: item},
        {type: TodoAddFormMutations.HIDE, payload: true}
      ], done)

      testAction(actions.save, null, emptyState, [], done)
    })
  })

})

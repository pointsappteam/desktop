import { GetterTree } from 'vuex'
import { State } from './state'

export const getters = {
  signupEmail (state: State): string {
    return state.authSignupForm.email
  },

  signupPassword (state: State): string {
    return state.authSignupForm.password
  },

  signupFormValid (state: State): boolean {
    return state.authSignupForm.isEmailValid && state.authSignupForm.isPasswordStrong
  },

  signupEmailValid (state: State): boolean {
    return state.authSignupForm.isEmailValid
  },

  passwordStrong (state: State): boolean {
    return state.authSignupForm.isPasswordStrong
  }
} as GetterTree<State, any>

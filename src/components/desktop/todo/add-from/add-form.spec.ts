import { spy, assert } from 'sinon'
import { expect } from 'chai'
import Component from 'vue-class-component'
import { ComponentTest } from '../../../../util/component-test'
import { TodoAddFormComponent } from './todo-add-form'

@Component({
  template: require('./add-form.html')
})
class MockTodoAddFormComponent extends TodoAddFormComponent {
  constructor () {
    super()
  }
}

class TestHelper {
  static openClass   = '.todo-list-add-btn'
  static formClass   = '.todo-list-add-form'
  static inputClass  = '.todo-list-add-form-title-input'
  static saveClass   = '.todo-list-add-form-add-btn'
  static cancelClass = '.todo-list-add-form-cancel-btn'

  static formExists (vm) {
    let form = vm.$el.querySelector(this.formClass)
    expect(form).to.exist
  }

  static formNotExists (vm) {
    let form = vm.$el.querySelector(this.formClass)
    expect(form).to.not.exist
  }

  static fillInput (vm) {
    let input = vm.$el.querySelector(this.inputClass) as HTMLInputElement
    input.value = "Test title"
    input.dispatchEvent(new Event("input"))
  }

  static clickOpen (vm) {
    let addBtn = vm.$el.querySelector(this.openClass) as HTMLAnchorElement
    addBtn.click();
  }

  static clickSave (vm) {
    let addBtn = vm.$el.querySelector(this.saveClass) as HTMLAnchorElement
    addBtn.click();
  }

  static clickCancel(vm) {
    let cancelBtn = vm.$el.querySelector(this.cancelClass) as HTMLAnchorElement
    cancelBtn.click();
  }

  static submitForm(vm) {
    let form = vm.$el.querySelector(this.formClass).childNodes[0] as HTMLFormElement
    let event = new Event('submit')

    form.dispatchEvent(event)
  }
}

describe('Add todo form component', () => {
  let directiveTest: ComponentTest

  beforeEach(() => {
    directiveTest = new ComponentTest('<div><testcomponent ref="component"></testcomponent></div>', { 'testcomponent': MockTodoAddFormComponent })
  })

  afterEach(() => {
    directiveTest.destroy();
  })

  describe('When clicking the add button', () => {
    beforeEach(async () => {
      directiveTest.createComponent()

      await directiveTest.execute((vm) => {
        document.body.appendChild(vm.$el) //do visibility and focus checks
        TestHelper.clickOpen(vm)
      })
    })

    it('form is visible and input has focus', async () => {
      await directiveTest.execute((vm) => {
        let form = vm.$el.querySelector(TestHelper.formClass)
        expect(form).to.exist
        expect(form.clientWidth).to.equal(640)
        expect(form.clientHeight).to.equal(130)

        let input = vm.$el.querySelector(TestHelper.inputClass)
        expect(input).to.equal(document.activeElement)
      })
    })

    it('form not closed on "Create" click with empty value', async () => {
      await directiveTest.execute((vm) => {
        TestHelper.clickSave(vm);
      })

      await directiveTest.execute((vm) => {
        TestHelper.formExists(vm)
      })
    })

    it('form closed on "Create" click with non-empty value', async () => {
      await directiveTest.execute((vm) => {
        TestHelper.fillInput(vm)
        TestHelper.clickSave(vm)
      })

      await directiveTest.execute((vm) => {
        TestHelper.formNotExists(vm)
      })
    })

    it('form closed on submit with non-empty value', async () => {
      await directiveTest.execute((vm) => {
        TestHelper.fillInput(vm)
        TestHelper.submitForm(vm)
      })

      await directiveTest.execute((vm) => {
        TestHelper.formNotExists(vm)
      })
    })

    it('form closed on "Cancel" click', async () => {
      await directiveTest.execute((vm) => {
        TestHelper.clickCancel(vm)
      })

      await directiveTest.execute((vm) => {
        TestHelper.formNotExists(vm)
      })
    })

    it('form clear on reopen after "Cancel" click', async () => {
      await directiveTest.execute((vm) => {
        TestHelper.fillInput(vm)
      })

      await directiveTest.execute((vm) => {
        TestHelper.clickCancel(vm)
      })

      await directiveTest.execute((vm) => {
        TestHelper.clickOpen(vm)
      })

      await directiveTest.execute((vm) => {
        let input = vm.$el.querySelector(TestHelper.inputClass) as HTMLInputElement
        expect(input.value).to.equal('')
      })
    })

    it('form closed on click outside', async () => {
      //click outside
      await directiveTest.execute((vm) => {
        let fakeElement = document.createElement('DIV')
        document.body.appendChild(fakeElement)
        fakeElement.click()
      })

      await directiveTest.execute((vm) => {
        TestHelper.formNotExists(vm)
      })
    })
  })

})

import { EventBus, TodoAddFormEvents } from '../../../events/event-bus'
import { MutationTree } from 'vuex'
import { TodoAddFormMutations } from '../../mutations'
import { State } from './state'

export const mutations = {
  [TodoAddFormMutations.SHOW] (state: State) {
    state.todoAddForm.active = true
    EventBus.$emit(TodoAddFormEvents.OPEN)
    EventBus.$forceUpdate()
  },
  [TodoAddFormMutations.HIDE] (state: State, clear: boolean) {
    state.todoAddForm.active = false

    if (clear) {
      state.todoAddForm.title = ''
    }
  },
  [TodoAddFormMutations.UPDATE_MESSAGE] (state: State, message: string) {
    state.todoAddForm.title = message
  }
} as MutationTree<State>

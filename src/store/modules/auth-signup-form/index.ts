import { State } from './state'
import { getters } from './getters'
import { mutations } from './mutations'
import { actions } from './actions'

export function makeAuthSignupForm (api) {
  return {
    state: new State(api),
    getters: getters,
    mutations: mutations,
    actions: actions
  }
}

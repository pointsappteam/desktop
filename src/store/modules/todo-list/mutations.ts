import { TodoListMutations } from '../../mutations'
import { EventBus, TodoListEvents } from '../../../events/event-bus'
import { ListItem } from '../../../model/todo/list-item'
import { MutationTree } from 'vuex'
import { State } from './state'

export const mutations = {
  [TodoListMutations.ADD] (state: State, item: ListItem) {
    state.todoList.items.push(item)
    EventBus.$emit(TodoListEvents.ADDED)
  },
  [TodoListMutations.CLEAR] (state: State, item: ListItem) {
    state.todoList.items = []
  },
  [TodoListMutations.ACTIVATE] (state: State, id: number) {
    state.setActiveById(id, true)
  },
  [TodoListMutations.DEACTIVATE] (state: State, id: number) {
    state.setActiveById(id, false)
  },
  [TodoListMutations.DEACTIVATE_ALL] (state: State, id: number) {
    state.deactivateAll()
  },
  [TodoListMutations.TOGGLE] (state: State, id: number) {
    state.toggleExclusiveById(id)
  },
  [TodoListMutations.TOGGLE_GROUP] (state: State, id: number) {
    state.toggleById(id)
  },
  [TodoListMutations.SWAP] (state: State, elements) {
    state.swap(elements.idX, elements.idY)
  },
  [TodoListMutations.DRAG_START] (state: State, id: number) {
    state.toggleDrag(id, true)
  },
  [TodoListMutations.DRAG_FINISH] (state: State, id: number) {
    state.toggleDrag(id, false)
  }
} as MutationTree<State>

import { Component, Vue } from 'vue-property-decorator'

import './add-form.scss'
import { Getter } from 'vuex-class'
import { mapActions, mapMutations } from 'vuex'
import { TodoAddFormMutations } from '../../../../store/mutations'
import { EventBus, TodoAddFormEvents } from '../../../../events/event-bus'
import { ClickOutsideDirective } from '../../../../util/directives/click-outside'
import { KeyCodes } from '../../../../util/constants'

@Component({
  template: require('./add-form.html'),
  components: {},
  methods: {
    ...mapActions(['open','close','closeClear', 'save'])
  },
  directives: {
    'click-outside': ClickOutsideDirective
  }
})

export class TodoAddFormComponent extends Vue {

  $refs: {
    title: HTMLInputElement
  }

  @Getter addFormActive: boolean
  @Getter addFormTitle: string

  onShow () {
    this.$nextTick(() => this.$refs.title.focus())
  }

  updateMessage (e) {
    this.$store.commit(TodoAddFormMutations.UPDATE_MESSAGE, e.target.value)
  }

  mounted () {
    let vm = this
    window.addEventListener('keyup', (event: KeyboardEvent) => {
      if (event.keyCode === KeyCodes.N && event.altKey === true) {
        vm.$store.commit(TodoAddFormMutations.SHOW)
      }
    })

    EventBus.$on(TodoAddFormEvents.OPEN, this.onShow)
  }

  beforeDestroy () {
    EventBus.$off(TodoAddFormEvents.OPEN)
  }
}

import Vue from 'vue'
import { makeHot, reload } from './util/hot-reload'
import { createRouter } from './router'
import { default as makeStore } from './store'
import { TokenLocalStorage } from './model/auth'
import { mockApi, standaloneApi } from './model/api'

// const navbarComponent = () => import('./components/navbar').then(({ NavbarComponent }) => NavbarComponent)
const menuComponent = () => import('./components/desktop/leftmenu').then(({ LeftmenuComponent }) => LeftmenuComponent)
const authComponent = () => import('./components/desktop/auth/auth').then(({ AuthComponent }) => AuthComponent)
// const navbarComponent = () => import(/* webpackChunkName: 'navbar' */'./components/navbar').then(({ NavbarComponent }) => NavbarComponent)

import './sass/main.scss'

if (process.env.ENV === 'development' && module.hot) {
  const menuModuleId = './components/desktop/leftmenu'

  // first arguments for `module.hot.accept` and `require` methods have to be static strings
  // see https://github.com/webpack/webpack/issues/5668

  makeHot(menuModuleId, menuComponent,
    module.hot.accept('./components/desktop/leftmenu', () => reload(menuModuleId, (require('./components/desktop/leftmenu') as any).LeftmenuComponent)))
}

let api

switch (process.env.API_MODE) {
  case 'standalone':
    api = standaloneApi
    break
  case 'mocked':
  default:
    api = mockApi
    break
}

// tslint:disable-next-line:no-unused-expression
new Vue({
  el: '#app-main',
  router: createRouter(),
  store: makeStore(new TokenLocalStorage(), api),
  components: {
    'leftmenu': menuComponent,
    'auth': authComponent
  }
})

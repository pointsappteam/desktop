export class ListItem {
  id: number
  title: string
  isActive: boolean
  isDragged: boolean
}

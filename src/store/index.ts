import Vue from 'vue'
import Vuex from 'vuex'
import { todoAddForm } from './modules/todo-add-form'
import { makeTodoList } from './modules/todo-list'
import { makeAuthModule } from './modules/auth'
import { makeAuthSignupForm } from './modules/auth-signup-form'
import { TokenStorageInterface } from '../model/auth'
import { ApiInterface } from '../model/api/api-interface'

const debug = process.env.NODE_ENV !== 'production'

Vue.use(Vuex)

export default function makeStore (tokenStorage: TokenStorageInterface, api: ApiInterface) {
  return new Vuex.Store({
    modules: {
      todoAddForm,
      todoList: makeTodoList(api),
      auth: makeAuthModule(tokenStorage),
      authSignupForm: makeAuthSignupForm(api)
    },
    strict: debug
  })
}

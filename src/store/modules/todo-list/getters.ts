import { ListItem } from '../../../model/todo/list-item'
import { GetterTree } from 'vuex'
import { State } from './state'

export const getters = {
  todoListItems (state: State): Array<ListItem> {
    return state.todoList.items
  }
} as GetterTree<State, any>

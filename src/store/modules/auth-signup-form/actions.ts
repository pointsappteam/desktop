import { ActionTree, ActionContext } from 'vuex'
import { AuthMutations } from '../../mutations'
import { State } from './state'
import axios from 'axios'

export const actions = {
  send (store: ActionContext<State, any>) {
    let data = {
      email: store.state.authSignupForm.email,
      password: store.state.authSignupForm.password
    }

    axios.post(store.state.api.signup, data)
      .then(
        (response) => {
          store.commit(AuthMutations.UPDATE_TOKEN, response.data.token)
        },
        (error) => { console.log(error) }
        )
  }
} as ActionTree<State, any>

import { Component, Vue } from 'vue-property-decorator'
import { AuthEvents, EventBus } from '../../../../events/event-bus'
import { mapActions, mapMutations } from 'vuex'

@Component({
  template: '<div></div>',
  components: {},
  methods: {
    ...mapActions(['restoreToken'])
  },
  directives: {
  }
})

export class AuthComponent extends Vue {

  onAuthorized () {
    this.$router.push('/')
  }

  onNotAuthorized () {
    this.$router.push('/signup/')
  }

  mounted () {
    EventBus.$on(AuthEvents.AUTHORISED, this.onAuthorized)
    EventBus.$on(AuthEvents.NOT_AUTHORISED, this.onNotAuthorized)

    this.$store.dispatch('restoreToken')
  }

  beforeDestroy () {
    EventBus.$off(AuthEvents.AUTHORISED)
    EventBus.$off(AuthEvents.NOT_AUTHORISED)
  }
}

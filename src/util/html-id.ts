export interface HtmlIdInterface {
  print (id: number)
  parse (elementId: string)
  fromElement (element: HTMLElement): string
  parseFromElement (element: HTMLElement): number
}

export class HtmlId implements HtmlIdInterface {
  private prefix: string

  constructor (prefix: string) {
    this.prefix = prefix
  }

  fromElement (element: HTMLElement) {
    let idElement = this.findIdentified(element)

    if (idElement === false) {
      return ''
    }

    return (idElement as HTMLElement).id
  }

  parse (elementId: string) {
    return parseInt(elementId.replace(this.prefix, ''), 10)
  }

  parseFromElement (element: HTMLElement): number {
    return this.parse(this.fromElement(element))
  }

  print (id: number) {
    return this.prefix + id.toString()
  }

  private findIdentified (element: HTMLElement): HTMLElement | boolean {
    if (element.id !== '') {
      return element
    }

    if (element.parentNode === null) {
      return false
    }

    return this.findIdentified(element.parentElement as HTMLElement)
  }

}

import { TokenStorageInterface } from '../../model/auth'
import { AuthEvents, EventBus } from '../../events/event-bus'
import { AuthMutations } from '../mutations'
import { ActionTree, ActionContext, GetterTree, MutationTree } from 'vuex'

// state
class AuthState {
  isAuthorized: boolean
  token: string
}

class State {
  auth = {
    isAuthorized: false,
    token: ''
  } as AuthState
  storage?: TokenStorageInterface = null
  constructor (storage: TokenStorageInterface) {
    this.storage = storage
  }
}

export function makeAuthModule (storage) {
  return {
    state: new State(storage),

    getters: {
      authorized (state: State): boolean {
        return state.auth.isAuthorized
      },

      authToken (state: State): string {
        return state.auth.token || ''
      }
    } as GetterTree<State, any>,

    mutations: {
      [AuthMutations.UPDATE_TOKEN] (state: State, token: string) {
        state.auth.token = token || ''

        state.auth.isAuthorized = (state.auth.token.length > 0)
        let event = state.auth.isAuthorized ? AuthEvents.AUTHORISED : AuthEvents.NOT_AUTHORISED

        state.storage.store(state.auth.token)

        EventBus.$emit(event)
        EventBus.$forceUpdate()
      }
    } as MutationTree<State>,

    actions: {
      restoreToken (store: ActionContext<State, any>) {
        let token = store.state.storage.get()
        store.commit(AuthMutations.UPDATE_TOKEN, token)
      }

    } as ActionTree<State, any>
  }
}

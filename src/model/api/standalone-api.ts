import { ApiInterface, ResourceApiInterface } from './api-interface'

class StandaloneApi implements ApiInterface {
  signup: string = 'https://127.0.0.1:8083/api/signup'
  points: ResourceApiInterface = {
    list: 'not-defined'
  } as ResourceApiInterface
}

export const standaloneApi = new StandaloneApi()

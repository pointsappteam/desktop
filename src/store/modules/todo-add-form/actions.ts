import { TodoAddFormMutations, TodoListMutations } from '../../mutations'
import { ActionTree, ActionContext } from 'vuex'
import { idGenerator } from '../../../util/id-generator'
import { ListItem } from '../../../model/todo/list-item'
import { State } from './state'

export const actions = {
  open (store: ActionContext<State, any>) {
    store.commit(TodoAddFormMutations.SHOW)
  },
  close (store: ActionContext<State, any>) {
    store.commit(TodoAddFormMutations.HIDE, false)
  },
  closeClear (store: ActionContext<State, any>) {
    store.commit(TodoAddFormMutations.HIDE, true)
  },
  save (store: ActionContext<State, any>) {
    const title = store.state.todoAddForm.title.trim()

    if (title === '') {
      return
    }

    let item = {
      id: idGenerator.generate('todo'),
      title: title,
      isActive: false,
      isDragged: false
    } as ListItem

    store.commit(TodoListMutations.ADD, item)
    store.commit(TodoAddFormMutations.HIDE, true)
  }
} as ActionTree<State, any>

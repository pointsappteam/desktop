export interface ApiInterface {
  signup: string
  points: ResourceApiInterface
}

export interface ResourceApiInterface {
  list: string
  create: string
  update: string
  delete: string
}

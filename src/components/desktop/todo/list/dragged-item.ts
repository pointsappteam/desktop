import { HtmlIdInterface } from '../../../../util/html-id'
import { Commit } from 'vuex'
import { TodoListMutations } from '../../../../store/mutations'
import { EventsToolkit } from '../../../../util/events-toolkit'

export class TodoListDraggedItem {

  private $storeCommit: Commit
  private idParser: HtmlIdInterface
  private container: HTMLElement

  private movedClone: HTMLDivElement = null
  private lastOver: HTMLDivElement = null
  private movedId = null

  private handle = { x: 0, y: 0 }

  constructor (storeCommit: Commit, idParser: HtmlIdInterface) {
    this.$storeCommit = storeCommit
    this.idParser = idParser

    this.container = document.getElementById('app-main')
  }

  exists (): boolean {
    return this.movedClone !== null
  }

  onMoveStart (e: MouseEvent) {
    if (e.buttons !== 1) {
      return
    }

    if (this.exists()) {
      return
    }

    let movedElement = document.getElementById(
      this.idParser.fromElement(e.toElement as HTMLElement)
    ) as HTMLDivElement

    process.nextTick(() => this.create(movedElement, e))

    EventsToolkit.preventAll(e)
  }

  create (movedElement: HTMLElement, e: MouseEvent) {
    let movedClone = movedElement.cloneNode(true) as HTMLDivElement

    movedClone.id += '-moved'

    movedClone.classList.add('moved')

    let newTop = e.clientY - e.offsetY

    this.handle.x = e.offsetX
    this.handle.y = e.offsetY

    movedClone.style.top = newTop + 'px'
    movedClone.style.left = (e.clientX - e.offsetX) + 'px'
    movedClone.style.width = getComputedStyle(movedElement).width

    this.container.appendChild(movedClone)
    console.log('bind')
    document.onmousemove = this.onMoveContainer.bind(this)
    document.onmouseup = this.onMouseUp.bind(this)

    this.movedClone = movedClone
    this.movedId = this.idParser.parse(movedElement.id)

    this.$storeCommit(TodoListMutations.DRAG_START, this.movedId)
    movedElement.style.height = getComputedStyle(movedClone).height
    this.lastOver = movedElement as HTMLDivElement
  }

  onMoveContainer (e: MouseEvent) {
    if (e.buttons !== 1) {
      if (this.movedClone !== null) {
        this.onMouseUp(e)
      }
      return
    }

    let newTop = e.clientY - this.handle.y
    let newLeft = e.clientX - this.handle.x
    let movedHeight = getComputedStyle(this.lastOver).height

    this.movedClone.style.top = newTop + 'px'
    this.movedClone.style.left = newLeft + 'px'

    let toElement = e.toElement as HTMLElement

    let toId = document.isSameNode(toElement.parentNode)
      ? null
      : this.idParser.parseFromElement(toElement)

    if (!isNaN(toId)) {
      if (this.movedId !== toId) {
        let underStyle = getComputedStyle(toElement)
        let underHeight = parseInt(underStyle.height, 10)
        let underPaddings = parseFloat(underStyle.paddingBottom) + parseFloat(underStyle.paddingTop)
        let noSwapMargin = underHeight * 0.2

        let depth = (e.movementY > 0) ? e.offsetY : (underHeight + underPaddings - e.offsetY)

        if (depth < noSwapMargin) {
          this.$storeCommit(TodoListMutations.SWAP, { idX: toId, idY: this.movedId })

          // adjust heights
          this.lastOver.style.height = underHeight + 'px'
          toElement.style.height = movedHeight
          this.lastOver = toElement as HTMLDivElement
        }
      }
    }

    EventsToolkit.preventAll(e)
  }

  onMouseUp (e: MouseEvent) {
    if (this.movedClone === null) {
      return
    }

    console.log('unbind')
    document.removeEventListener('mousemove', this.onMoveContainer)
    document.removeEventListener('mouseup', this.onMouseUp)
    document.getElementById('app-main').removeChild(this.movedClone)

    let draggedId = this.movedId

    this.movedClone = null
    this.movedId = null

    EventsToolkit.preventAll(e)

    process.nextTick(() => {
      this.$storeCommit(TodoListMutations.DRAG_FINISH,draggedId)
    })
  }

}
